'use strict';

var devicesController = function () {
    var logger = require('winston');
    var Device = require('../models/deviceSchema');

    /*
     * get list of devices from database
     */
    
    function getDevices(req, res, next){
        logger.debug('[devicesController] getDevices()');

        Device.find({}, function(err, devicesList) {
            if(err){
                logger.error(err);
                return err;
            }

            res.send(devicesList);
        });
    }

    /*
     * remove existing device from database
     */

    function removeExistingDevice (req, res, next) {
        logger.debug('[deviceController] removeExistingDevice()');
        //logger.debug(req.body);

        var id = null;

        if(req.method == 'GET'){
            id = req.query.id;
        }
        else if(req.method == 'POST'){
            id = req.body.id;
        }
        else {
            return false;
        }

        Device.remove({id: id}, function(err, result) {
            if(err){
                logger.error(err);
                throw err;
            }
            logger.debug('Existing rule removed');

            res.sendStatus(200);
        });
    }

    return {
        getDevices: getDevices,
        removeExistingDevice: removeExistingDevice
    };
};

module.exports = devicesController();