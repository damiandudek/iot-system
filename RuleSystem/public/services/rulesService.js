app.service('rulesService', function ($http, $q, $location) {

    var protocol = $location.protocol();
    var host = $location.host();
    var port = $location.port();
    var serverIP = protocol + '://' + host + ':' + port + '/rules';

    var rules = [];

    var handleError = function( response ) {
        if (! angular.isObject( response.data ) || ! response.data.message) {
            return( $q.reject( "An unknown error occurred." ) );
        }
        return( $q.reject( response.data.message ) );
    };

    var handleSuccess = function ( response ) {
        return( response.data );
    };

    var getRules = function getRules () {
        console.log("getRules()");
        var request = $http({
            method: "get",
            url: serverIP + "/getRules",
            params: {}
        });

        return( request.then( function handleSuccess( response ) {
            rules = response.data;
            return( response.data );
        }, handleError ) );
    };

    var addRule = function (ruleObj) {
        console.log("addRule()");

        $.ajax({
            type: 'post',
            url: serverIP + "/saveNewRule",
            data: JSON.stringify(ruleObj),
            contentType: "application/json; charset=utf-8",
            traditional: true,
            success: function (data) {
                console.log("Success to save new rule");
            },
            error: function(error){
                console.log(error);
            }
        });
    };

    var deleteRule = function (id) {
        var request = $http({
            method: "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: serverIP + "/removeExistingRule",
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {id: id}
        });

        return ( request.then(function () {
            // on success
            // refresh after success response
            $location.url('/removeExistingRule');
        }, handleError) );
    };

    var getRule = function (id) {
        for (var i = 0, length = rules.length; i < length; i++){
            if (rules[i]._id === id){
                return rules[i];
            }
        }
        return null;
    };

    var enableRule = function (id) {
        var request = $http({
            method: "get",
            url: serverIP + "/enableRule",
            params: {
                id: id
            }
        });

        return( request.then( handleSuccess, handleError ) );
    };

    var disableRule = function (id) {
        var request = $http({
            method: "get",
            url: serverIP + "/disableRule",
            params: {
                id: id
            }
        });

        return( request.then( handleSuccess, handleError ) );
    };

    var fireRule = function (id) {
        var request = $http({
            method: "get",
            url: serverIP + "/fireRule",
            params: {
                id: id
            }
        });

        return( request.then( handleSuccess, handleError ) );
    };

    var fireAllPeriodicRules = function () {
        var request = $http({
            method: "get",
            url: serverIP + "/fireAllPeriodicRules",
            params: {}
        });

        return( request.then( handleSuccess, handleError ) );
    };

    var stopAllPeriodicRules = function () {
        var request = $http({
            method: "get",
            url: serverIP + "/stopAllPeriodicRules",
            params: {}
        });

        return( request.then( handleSuccess, handleError ) );
    };

    // return public API
    return {
        getRules: getRules,
        getRule: getRule,
        addRule: addRule,
        deleteRule: deleteRule,
        enableRule: enableRule,
        disableRule: disableRule,
        fireRule: fireRule,
        fireAllPeriodicRules: fireAllPeriodicRules,
        stopAllPeriodicRules:stopAllPeriodicRules
    };
});