var db = require('mongoose');
var Schema = db.Schema;
var ObjectId = db.ObjectId;

var ruleSchema = new Schema ({
        name: String,
        description: String,
        isEnabled: Boolean,
        antecedent: Schema.Types.Mixed,
        antecedentData: Schema.Types.Mixed,
        consequent: Schema.Types.Mixed,
        runEvery: Number
    },
    {
        strict: false
    });

module.exports = db.model('rules', ruleSchema);