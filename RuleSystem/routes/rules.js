var express = require('express');
var router = express.Router();
var controller = require('../controllers/rulesController');

router.get('/', controller.getRules);
router.get('/getRules', controller.getRules);

router.get('/getRule', controller.getRule);

router.post('/saveNewRule', controller.saveNewRule);

router.post('/removeExistingRule', controller.removeExistingRule);

router.get('/enableRule', controller.enableRule);
router.get('/disableRule', controller.disableRule);

router.get('/fireRule', controller.fireRule);
router.get('/fireAllPeriodicRules', controller.fireAllPeriodicRules);
router.get('/stopAllPeriodicRules', controller.stopAllPeriodicRules);

module.exports = router;
