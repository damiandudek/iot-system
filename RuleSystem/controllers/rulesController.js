var rulesController = function () {
    var logger = require('winston');
    var Rule = require('../models/ruleSchema');
    var RuleEngine = require('../RuleEngine');

    /*
     * returns list of rules in response
     */

    function getRules (req, res, next) {
        logger.debug('[rulesController] getRules()');

        RuleEngine.loadRules()
            .then(function (rulesList) {
                logger.debug('[rulesController] Success to load rules from DB.');
                res.send(rulesList);
                return rulesList;
            })
            .catch(function (error) {
                logger.error(error);
                logger.error('Failed to load rules from DB.');
                return false;
            });
    }

    /*
     * returns single rule in response
     */

    function getRule (req, res, next) {
        logger.debug('[rulesController] getRule()');

        var rule;

        if(rule = RuleEngine.getRule(req.query.id))
            res.send(rule);
        else {
            logger.debug('Rule ' + req.query.id + ' not exist.');
        }
    }

    /*
     * save new rule in database
     */

    function saveNewRule (req, res, next) {
        logger.debug('[rulesController] saveNewRule()');
        //logger.debug(req.body);

        if(RuleEngine.saveNewRule(req.body)){
            res.sendStatus(200);
        }
    }

    /*
     * remove existing rule from database
     */

    function removeExistingRule(req, res, next){
        logger.debug('[rulesController] removeExistingRule()');

        // if remove request is send by GET then instead of req.body.id use req.query.id

        RuleEngine.removeExistingRule(req.body.id)
            .then(function (data) {
                if(data.result.n < 1){
                    logger.error('Failed to remove selected rule. Probably rule doesn\'t exist');
                    res.sendStatus(404);
                    return;
                }
                res.sendStatus(200);
            })
            .catch(function (error) {
                logger.error(error);
                logger.error('Failed to remove selected rule.');
            });
    }

    /*
     * enable rule
     */

    function enableRule (req, res, next) {
        logger.debug('[rulesController] enableRule()');

        RuleEngine.enableRule(req.query.id)
            .then(function (data) {
                logger.debug('[rulesController] Success to enable rule');
                res.sendStatus(200);
            })
            .catch(function (error) {
                logger.error(error);
                logger.error('Failed to enable selected rule.');
            });
    }

    /*
     * disable rule
     */

    function disableRule (req, res, next) {
        logger.debug('[rulesController] disableRule()');

        RuleEngine.disableRule(req.query.id)
            .then(function (data) {
                logger.debug('[rulesController] Success to disable rule');
                res.sendStatus(200);
            })
            .catch(function (error) {
                logger.error(error);
                logger.error('[rulesController] Failed to disable selected rule.');
            });
    }

    /*
     * fire single rule
     */

    function fireRule (req, res, next) {
        RuleEngine.fireRule(req.query.id);
        res.sendStatus(200);
    }

    /*
     * fire periodic rules
     */

    function fireAllPeriodicRules (req, res, next) {
        RuleEngine.firePeriodicRules();
        res.sendStatus(200);
    }

    /*
     * stop periodic rules
     */

    function stopAllPeriodicRules (req, res, next) {
        RuleEngine.stopAllPeriodicRules();
        res.sendStatus(200);
    }

    return {
        getRules: getRules,
        getRule: getRule,
        saveNewRule: saveNewRule,
        removeExistingRule: removeExistingRule,
        enableRule: enableRule,
        disableRule: disableRule,
        fireRule: fireRule,
        fireAllPeriodicRules: fireAllPeriodicRules,
        stopAllPeriodicRules: stopAllPeriodicRules
    }
};

module.exports = rulesController();