app.controller('RulesController', function($scope, rulesService, devicesService) {
    init();

    // initialize data
    function init(){
        $scope.rules = [];
        $scope.devices = [];

        $scope.newRule = {
            name: null,
            description: null,
            isEnabled: true,
            antecedent: {},
            antecedentData: null,
            consequent: {},
            runEvery: null
        };

        devicesService.getDevices()
            .then(
            function ( data ) {
                console.log("Devices:");
                console.log(data);

                $scope.devices = data;
                return data;
            }
        );

        rulesService.getRules()
            .then(
            function( data ) {
                console.log("Rules:");
                console.log(data);

                $scope.rules = data;
                return data;
            }
        );

    }

    $scope.operators = [
        'none',
        '==',
        '>',
        '<',
        '>=',
        '<=',
        '!='
    ];

    $scope.processRule = function (){
        var condition = {};
        var antecedentData = [];

        if($scope.newRule.skipAntecedent === true){
            $scope.skipAntecedent();
        }

        if($scope.operation === 'none'){
            condition = {};
            $scope.newRule.antecedent = {};
            $scope.newRule.antecedentData = [$scope.newRule.antecedentData];
        }
        if($scope.operation === 'toConst'){
            condition = {};

            condition[$scope.newRule.antecedent.operator] = [ {"var" : "var1"}, $scope.newRule.antecedent.referenceValue];
            antecedentData = [$scope.newRule.antecedentData];
        }
        if($scope.operation === 'toFunctionResult'){
            condition = {};

            condition[$scope.newRule.anotherDevice.operator] = [ { "var" : "var1"}, { "var" : "var2"} ];
            antecedentData = [$scope.newRule.antecedentData, {deviceId: $scope.newRule.anotherDevice.deviceId, functionToCall: $scope.newRule.anotherDevice.functionToCall}]
        }
        if($scope.newRule.compareToResultOfOtherCondition){
            condition = {};
            var newRuleCondition = {};

            var existingRule = rulesService.getRule($scope.newRule.existingRule.id);

            // convert array to string, find number of 'var' words and return half of this value
            // g switch is to return an array of matched elements
            var numberOfVars = ((JSON.stringify(existingRule).match(/var/g) || []).length) / 2;

            newRuleCondition[$scope.newRule.antecedent.operator] = [{ "var" : "var" + numberOfVars}, $scope.newRule.antecedent.referenceValue];

            condition[$scope.newRule.logicalExpression] = [existingRule.antecedent];
            antecedentData = [$scope.newRule.antecedentData].concat(existingRule.antecedentData);
        }

        // get params for selected function
        $scope.newRule.consequent.params = $scope.devices[$scope.selected.consequentDeviceIndex].api[$scope.selected.consequentFunctionIndex].params;

        var newRuleObj = {
            name: $scope.newRule.name,
            description: $scope.newRule.description,
            isEnabled: $scope.newRule.isEnabled,
            antecedent: condition,
            antecedentData: antecedentData,
            consequent: $scope.newRule.consequent,
            runEvery: $scope.newRule.runEvery > 0 ? $scope.newRule.runEvery : 0
        };

        rulesService.addRule(newRuleObj);

        alert('New rule saved!');

        $scope.clearAllData();
    };

    $scope.skipAntecedent = function () {
        $scope.newRule.antecedentData.deviceId = -1;
        $scope.newRule.antecedentData.functionToCall = null;
        $scope.operation = null;
        $scope.newRule.antecedent = { "==" : [1, 1] };
        $scope.newRule.antecedentData = null;
        $scope.newRule.antecedent.operator = null;
        $scope.selected.antecedentDeviceIndex = -1;
        $scope.selected.antecedentFunctionIndex = -1;
        $scope.show.operationBox = false;
        $scope.show.comparisonBox = false
    };

    $scope.clearAllData = function () {
        $scope.newRule.name = null;
        $scope.newRule.description = null;
        $scope.newRule.isEnabled = null;
        $scope.newRule.isPeriodic = null;
        $scope.newRule.runEvery = null;
        $scope.newRule.skipAntecedent = null;
        $scope.newRule.antecedent = {};
        $scope.newRule.antecedentData = null;
        $scope.newRule.consequent = {};
        $scope.operation = null;
        $scope.selected.antecedentDeviceIndex = -1;
        $scope.selected.antecedentFunctionIndex = -1;
        $scope.selected.consequentDeviceIndex = -1;
        $scope.selected.consequentFunctionIndex = -1;
        $scope.show.operationBox = false;
        $scope.show.comparisonBox = false

    };

    $scope.getRule = function (id){
        rulesService.getRule(id);
    };

    $scope.deleteRule = function (id) {
        if (confirm("Are you sure you want to delete this rule?")) {
            rulesService.deleteRule(id);
            init();
        }
    };

    $scope.enableRule = function (id){
        rulesService.enableRule(id);
        init();
    };

    $scope.disableRule = function (id){
        rulesService.disableRule(id);
        init();
    };

    $scope.fireRule = function (id){
        rulesService.fireRule(id);
    };

    $scope.fireAllPeriodicRules = function (id){
        rulesService.fireAllPeriodicRules(id);
    };

    $scope.stopAllPeriodicRules = function (id){
        rulesService.stopAllPeriodicRules(id);
    };

    $scope.getDevice = function (id){
        devicesService.getDevice(id);
    };
});

app.controller('WebInterfacesController', function($scope, devicesService) {
    init();

    function init() {
        $scope.WebInterfaces = [];

        devicesService.getDevices();
        var devices = devicesService.getDevices()
            .then(
            function ( devicesList ) {
                for(var index in devicesList){
                    $scope.WebInterfaces.push({name: devicesList[index].name, url: devicesList[index].webUserInterfaceURL, ip:devicesList[index].ip, port:devicesList[index].port});
                }
            }
        );
    }
    $scope.WebInterface = $scope.WebInterfaces[0];
});