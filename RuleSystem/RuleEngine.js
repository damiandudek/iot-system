'use strict';

var RuleEngine = function () {
    var logger = require('winston');
    var fs = require('fs');

    var loader = require('./loader');
    var devicesControllers = loader.devicesControllers;

    var Rule = require('./models/ruleSchema');

    var jsonLogic = require('json-logic-js');

    var rules = [];
    var periodicRules = [];
    var intervalsIDs = [];

    /*
     * load rules from database
     */

    var loadRules = function () {
        logger.debug('[RuleEngine] loadRules()');

        return new Promise(function (resolve, reject){
            rules = [];
            periodicRules = [];

            Rule.find({}, function (err, rulesList) {
                if (err) reject(err);
                else {
                    logger.debug('Load rules:');

                    for (var i in rulesList) {
                        if (rulesList[i]._id === undefined) {
                            continue;
                        }
                        if (rulesList[i].runEvery > 0) {
                            logger.debug('ID: ' + rulesList[i]._id + '(periodic rule)');
                            periodicRules.push(rulesList[i]);
                        }
                        else {
                            logger.debug('ID: ' + rulesList[i]._id);
                            rules.push(rulesList[i]);
                        }
                    }
                    resolve(rulesList);
                }
            });
        });
    };

    /*
     * save new rule to database
     */

    var saveNewRule = function (rule) {
        logger.debug('[RuleEngine] saveNewRule()');

        var newRuleToSave = new Rule({
            name: rule.name,
            description: rule.description,
            isEnabled: rule.isEnabled,
            antecedent: rule.antecedent,
            antecedentData: rule.antecedentData,
            consequent: rule.consequent,
            runEvery: rule.runEvery
        });

        newRuleToSave.save(function (err, data) {
            if (err) {
                logger.error(err);
                return err;
            }
            else {
                logger.debug('New rule saved.');
                logger.debug(data);
                return true;
            }
        });
    };

    /*
     * remove existing rule from database
     */

    var removeExistingRule = function (ruleId) {
        logger.debug('[RuleEngine] removeExistingRule()');
        logger.debug('Trying to remove: ' + ruleId);

        return new Promise(function (resolve, reject){
            Rule.remove({_id: ruleId}, function (err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    };

    /*
     * enable rule
     */

    var enableRule = function (ruleId) {
        logger.debug('[RuleEngine] enableRule()');

        return new Promise(function (resolve, reject) {
            Rule.update({_id: ruleId}, {$set: {isEnabled: true}}, function (err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    };

    /*
     * disable rules
     */
    var disableRule = function (ruleId) {
        logger.debug('[RuleEngine] disableRule()');

        return new Promise(function (resolve, reject){
            Rule.findByIdAndUpdate({_id: ruleId}, {$set: {isEnabled: false}}, function (err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    };

    /*
     * get rule
     */

    var getRule = function (ruleId) {
        logger.debug('[RuleEngine] getRule()');

        for(var i = 0, len = rules.length; i < len; i++){
            if(rules[i]._id === ruleId){
                return rules[i];
            }
        }
        return false;
    };

    /*
     * get number of rules
     */

    var getNumberOfRules = function () {
        logger.debug('[RuleEngine] getNumberOfRules()');

        return rules.length;
    };

    /*
     * get periodic rules
     */

    var getPeriodicRules = function () {
        logger.debug('[RuleEngine] getPeriodicRules()');

        return periodicRules;
    };

    /*
     * get periodic rule
     */

    var getPeriodicRule = function () {
        logger.debug('[RuleEngine] getPeriodicRule()');

        return periodicRules;
    };

    /*
     * get number of periodic rules
     */

    var getNumberOfPeriodicRules = function () {
        logger.debug('[RuleEngine] getNumberOfPeriodicRules()');

        return periodicRules.length;
    };

    /*
     * process the rule
     */

    var processRule = function (rule) {
        logger.debug('[RuleEngine] processRule()');
        logger.debug('[RuleEngine] Fire rule: ' + rule._id);

        if (rule.isEnabled != true) {
            logger.debug('[RuleEngine] Rule cannot be fired. Rule is inactive  [ruleId: ' + rule._id + '].');
            return false;
        }

        if (rule.antecedentData !== undefined && rule.antecedent !== undefined) {

            var promises = [];
            var data = {};

            for (var i = 0, len = rule.antecedentData.length; i < len; i++) {
                var params = rule.antecedentData[i].params ? rule.antecedentData[i].params : [];

                logger.debug('[RuleEngine] deviceId: ' + rule.antecedentData[i].deviceId);
                logger.debug('[RuleEngine] functionToCall: ' + rule.antecedentData[i].functionToCall);
                logger.debug('[RuleEngine] params: ' + params);

                promises.push(new Promise(function (resolve, reject) {
                    var callback = function (result){
                        if(result !== undefined){
                            logger.debug('Success');
                            logger.debug(result);
                            resolve(result);
                        } else {
                            logger.error('Error');
                            logger.error(result);
                            reject(result);
                        }
                    };
                    params.push(callback);
                    devicesControllers[rule.antecedentData[i].deviceId][rule.antecedentData[i].functionToCall].apply(this, params);
                }));
            }

            Promise.all(promises)
                .then(function(results) {
                    var consequentFunctionParams = [];
                    var expressionResult = false;

                    for(var propName in results){
                        data['var' + propName] = results[propName];
                    }
                    expressionResult = jsonLogic.apply(rule.antecedent, data);

                    logger.debug('[RuleEngine] Expression result: ' + expressionResult);

                    // run consequent
                    if (expressionResult === true) {

                        var consequentCallback = function (result){
                            if(result !== undefined && result !== null){
                                logger.debug('Success');
                                if(typeof result === 'function'){
                                    result();
                                } else {
                                    logger.debug(result);
                                }
                            } else {
                                logger.debug('Error');
                                logger.debug(result);
                            }
                        };

                        consequentFunctionParams = rule.consequent.params;// ? rule.consequent.params : null;
                        consequentFunctionParams.push(consequentCallback);
                        devicesControllers[rule.consequent.deviceId][rule.consequent.functionToCall].apply(this, consequentFunctionParams);
                        consequentFunctionParams.pop();
                        return true;
                    }
                    else {
                        logger.error('[Rule Engine] fire() | Rule not fired. Antecedent is false ');
                        logger.error('[Rule Engine] fire() | Rule not have all required fields setup correctly to be fired.');
                        return false;
                    }
                })
                .catch(function(error){
                    logger.error('Error');
                    logger.error(error);
                });
        }
    };

    /*
     * fire the rule
     */
    var fireRule = function (ruleId) {
        logger.debug('[RuleEngine] fireRule()');
        var allRules = rules.concat(periodicRules);
        var rule = {};

        for (var i in allRules) {
            if (allRules[i]._id == ruleId) {
                rule = allRules[i];
                break;
            }
        }

        return processRule(rule)
    };

    /*
     * fire all rules
     */

    var fireAllRules = function () {
        logger.debug('[RuleEngine] fireAllRules()');

        if (rules.length < 1) {
            logger.debug('There are no rules to be fired.');
            return false;
        }

        for (var i in rules) {
            processRule(rules[i]);
        }
    };

    /*
     * fire periodic rules
     */

    var firePeriodicRules = function () {
        logger.debug('[RuleEngine] firePeriodicRules()');

        if (periodicRules.length < 1) {
            logger.debug('There are no periodic rules to be fired.');
            return false;
        }

        var rule = {};

        for (var index in periodicRules) {
            rule = periodicRules[index];

            if (rule.isEnabled != true) {
                logger.debug('This periodic rule is inactive and cannot be fired [ruleId: ' + rule._id + ']');
                continue;
            }

            var intervalId = setInterval(function (rule, index) {
                logger.debug('[firePeriodicRules][runEvery: ' + rule.runEvery + ']: ' + rule.name);

                processRule(rule);
            }, rule.runEvery, rule);

            intervalsIDs.push({
                id: periodicRules[index].id,
                intervalId: intervalId
            });
        }
    };

    /*
     * stop periodic rule
     */
    var stopPeriodicRule = function (periodicRuleID) {
        logger.debug("[RuleEngine] stopPeriodicRule()");
        if (intervalsIDs.length < 1) {
            logger.debug('There are no run periodic rules');
            return false;
        }

        for (var index in intervalsIDs) {
            if (intervalsIDs[index].id == periodicRuleID) {
                clearInterval(intervalsIDs[index].intervalId);
                intervalsIDs.splice(index, 1);
            }
        }
    };

    /*
     * stop all periodic rules
     */

    var stopAllPeriodicRules = function () {
        logger.debug("[RuleEngine] stopAllPeriodicRules()");

        if (intervalsIDs.length < 1) {
            logger.debug('There are no run periodic rules');
            return false;
        }

        for (var index in intervalsIDs) {
            clearInterval(intervalsIDs[index].intervalId);
        }

        intervalsIDs = [];
    };

    /*
     * return public API
     */

    return {
        loadRules: loadRules,
        saveNewRule: saveNewRule,
        removeExistingRule: removeExistingRule,
        enableRule: enableRule,
        disableRule: disableRule,
        getNumberOfRules: getNumberOfRules,
        getRule: getRule,
        getPeriodicRules: getPeriodicRules,
        getPeriodicRule: getPeriodicRule,
        getNumberOfPeriodicRules: getNumberOfPeriodicRules,
        fireRule: fireRule,
        fireAllRules: fireAllRules,
        firePeriodicRules: firePeriodicRules,
        stopPeriodicRule: stopPeriodicRule,
        stopAllPeriodicRules: stopAllPeriodicRules,
    };
};

module.exports = RuleEngine();