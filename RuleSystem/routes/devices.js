var express = require('express');
var router = express.Router();
var controller = require('../controllers/devicesController');

router.get('/', controller.getDevices);
router.get('/getDevices', controller.getDevices);

router.get('/removeExistingDevice', controller.removeExistingDevice);
router.post('/removeExistingDevice', controller.removeExistingDevice);

module.exports = router;