var app = angular.module('RulesManagerApp', ['ngRoute','ngAnimate']);

/*
 * Routing
 */

// Configure routes and associate routes with views
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'RulesController',
            templateUrl: '/views/rulesList.html'
        })
        .when('/rulesList', {
            controller: 'RulesController',
            templateUrl: '/views/rulesList.html'
        })
        .when('/addRule/', {
            controller: 'RulesController',
            templateUrl: '/views/addRule.html'
        })
        .when('/manualControl/', {
            controller: 'RulesController',
            templateUrl: '/views/manualControl.html'
        })
        .otherwise({ redirectTo: '/'});
});

// enable CORS (Cross-Origin Resource Sharing) - when angular is working on the same machine as server app
app.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);