/*
 * DHT11 configuration file
 */

var DHT11ControllerConfig = {
    id: "57fba92d600f1338ea8598de",
    name: "DHT11Controller",
    description: "Temperature and humidity sensor",
    ip: "169.254.14.2",
    port: 80,
    api: [
        {
            name: "getTemperature",
            title: "Get temperature readings",
            description: "Function returns temperature readings from DHT11 sensor"
        },
        {
            name: "getHumidity",
            title: "Get humidity readings",
            description: "Function returns humidity readings from DHT11 sensor"
        }
    ],
    webUserInterfaceURL: "webInterfaces/dht11controller.html"
};

module.exports = DHT11ControllerConfig;