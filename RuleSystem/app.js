'use strict';

// include express application
var express = require('express');

// path module provides utilities for working with file and directory paths
var path = require('path');

// serve-favicon allows to cache app icon in memory
// to improve performance by skipping disk access
var favicon = require('serve-favicon');

// morgan module is HTTP request logger middleware
var morganLogger = require('morgan');

var cookieParser = require('cookie-parser');

// body-parse is a middleware that parse incoming request bodies
// available under the req.body property
var bodyParser = require('body-parser');

// helmet is a middleware that helps to protect applications from some well-known web vulnerabilities
// helmet is a collection of nine middleware functions that set security-related HTTP headers
var helmet = require('helmet');

/*
 * Routing
 */
var routes = require('./routes/index');
var rulesRouter = require('./routes/rules');
var devicesRouter = require('./routes/devices');

/*
 * Database connection
 */
var databaseAddress = 'mongodb://localhost:27017/iotdb';
var db = require('mongoose');

// open MongoDB pooling connections
db.connect(databaseAddress);

/*
 * Winston logger
 */
var logger = require('winston');
//logger.add(logger.transports.File, { filename: 'logs.log' });
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'info',
    prettyPrint: true,
    colorize: true,
    silent: false,
    timestamp: false
});
logger.cli();

/*
 * RuleEngine
 */
var RuleEngine = require('./RuleEngine.js');

/*
 * Express.js configuration
 */

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// default engine must be specified
app.set('view engine', 'jade');

app.use(helmet());
app.use(favicon(path.join(__dirname, 'public', 'images', 'favicon.ico')));
app.use(morganLogger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

// set path to static pages
app.use(express.static(path.join(__dirname, 'public')));

// a middleware to enable CORS (Cross-Origin Resource Sharing) for every response
app.use(function(req, res, next) {
    // add automatically this headers to every response
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/*
 * Routing
 */
app.use('/', routes);
app.use('/rules', rulesRouter);
app.use('/devices', devicesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

RuleEngine.loadRules();


module.exports = app;

app.listen(3000, '0.0.0.0', function () {
    logger.info('App listening on port 3000!');
});


