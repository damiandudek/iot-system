/*
 * Remote Controller configuration file
 */

var RCSwithControllerConfig = {
    id: "57fba92d600f1338ea8598dd",
    name: "RCSwitchController",
    description: "Control switches via 433MHz frequency",
    ip: "169.254.14.2",
    port: 80,
    api: [
        {
            name: "sendSignaltoSwitch",
            title: "Turn Switch 1 ON",
            description: "Trigger action: turn switch 1 on",
            params: [1, 1]
        },
        {

            name: "sendSignaltoSwitch",
            description: "Trigger action: turn switch 1 off",
            title: "Turn Switch 1 OFF",
            params: [1, 0]

        },
        {
            name: "sendSignaltoSwitch",
            description: "Trigger action: turn switch 2 on",
            title: "Turn Switch 2 ON",
            params: [2, 1]
        },
        {
            name: "sendSignaltoSwitch",
            description: "Trigger action: turn switch 2 off",
            title: "Turn Switch 2 OFF",
            params: [2, 0]
        },
        {
            name: "sendSignaltoSwitch",
            description: "Trigger action: turn switch 3 on",
            title: "Turn Switch 3 ON",
            params: [3, 1]
        },
        {
            name: "sendSignaltoSwitch",
            description: "Trigger action: turn switch 3 off",
            title: "Turn Switch 3 OFF",
            params: [3, 0]
        }
    ],
    "webUserInterfaceURL": "webInterfaces/remoteController.html"
};

module.exports = RCSwithControllerConfig;