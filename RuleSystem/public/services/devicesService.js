app.service('devicesService', function ($http, $q, $location) {

    var protocol = $location.protocol();
    var host = $location.host();
    var port = $location.port();
    var serverIP = protocol + '://' + host + ':' + port + '/devices';

    function handleError( response ) {
        if (! angular.isObject( response.data ) || ! response.data.message) {
            return( $q.reject( "An unknown error occurred." ) );
        }
        return( $q.reject( response.data.message ) );
    }

    function handleSuccess( response ) {
        return( response.data );
    }

    this.getDevices = function () {

        //return devices;
        var request = $http({
            method: "get",
            url: serverIP + "/getDevices",
            params: {}
        });

        return( request.then( handleSuccess, handleError ) );
    };

    this.deleteDevice = function (id) {
        var request = $http({
            method: "POST",
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            url: serverIP + "/removeExistingDevice",
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: {id: id}
        });

        return ( request.then(function () {
            // on success
        }, handleError) );
    };

    this.getDevice = function (id) {
        for (var i = 0, length = devices.length; i < length; i++) {
            if (devices[i].id === id) {
                return devices[i];
            }
        }
        return null;
    }
});