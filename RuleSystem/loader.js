var loader = function () {
    var fs = require('fs');
    var path = require('path');

    var directories = fs.readdirSync(path.join(__dirname, 'components'));

    var devicesConfiguration = [];
    var devicesControllers = [];

    for (var i = 0, len = directories.length; i < len; i++) {
        var pathToComponentDir = path.join(__dirname, 'components', directories[i]);
        devicesConfiguration.push(require(path.join(pathToComponentDir, 'config.js')));
        devicesControllers[devicesConfiguration[i].id] = require(path.join(pathToComponentDir, 'index.js'));
    }

    return {
        devicesConfiguration: devicesConfiguration,
        devicesControllers: devicesControllers
    }
};

module.exports = loader();