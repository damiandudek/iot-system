'use strict';

/*
 * Remote Controller driver
 */

var RemoteController = (function () {
    var RemoteControllerConfig = require('./config.js');
    var http = require('http');
    var logger = require('winston');

    http.shouldKeepAlive = false;
    http.globalAgent.maxSockets = 100;

    var moduleName = "RCSwitchController";

    function _sendSignaltoSwitch(switchNumber, state, callback) {
        logger.debug("[RemoteController] sendSignaltoSwitch() args: [switchNumber: " + switchNumber + ", signal: " + state +']');

        if(typeof switchNumber !== 'number'){
            // or display error
            logger.error("Wrong switchNumber");
            return false;
        }

        if(typeof state !== 'number'){
            // or display error
            logger.error("Wrong signal number");
            return false;
        }

        var onResponseCallback = function (response) {
            var body = [];

            logger.debug('STATUS: ' + response.statusCode);
            logger.debug('HEADERS: ' + JSON.stringify(response.headers));

            response.setEncoding('utf8');
            response.on('data', function (chunk) {
                logger.debug('[onData]');
                body.push(chunk);
            });
            response.on('end', function() {
                logger.debug("[onEnd]");
                logger.debug('Response BODY: \n' + body);
                callback(true);
            });
        };

        if(switchNumber === 1 && state === 1)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch1on', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
        if(switchNumber === 1 && state === 0)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch1off', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
        if(switchNumber === 2 && state === 1)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch2on', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
        if(switchNumber === 2 && state === 0)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch2off', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
        if(switchNumber === 3 && state === 1)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch3on', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
        if(switchNumber === 3 && state === 0)
            http.get('http://' + RemoteControllerConfig.ip + '/?switch3off', onResponseCallback).on('error', function (e) {
                logger.error("[onError]: " +  e.message);
            });
    }

    // Expose API

    return {
        sendSignaltoSwitch: _sendSignaltoSwitch
    };
}) ();

module.exports = RemoteController;