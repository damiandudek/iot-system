var db = require('mongoose');
var Schema = db.Schema;

var deviceSchema = new Schema({
        id: String,
        name: String,
        description: String,
        ip: String,
        port: Number,
        api: Schema.Types.Mixed,
        webUserInterfaceURL: String
    },
    {
        strict: false
    });

module.exports = db.model('devices', deviceSchema);