'use strict';

/*
 * DHT11 Controller driver
 */

var DHT11Controller = (function () {
    var DHT11ControllerConfig = require('./config.js');
    var http = require('http');
    var logger = require('winston');

    http.shouldKeepAlive = false;
    http.globalAgent.maxSockets = 100;

    var moduleName = "DHT11Controller";

    /*
     * send http request and returns results from the sensor
     */

    var _getReadingsFromSensor = function(options) {

        // return new pending promise
        return new Promise(function (resolve, reject) {
            var request = http.get(options, function(response) {

                if (response.statusCode < 200 || response.statusCode > 299) {
                    reject(new Error('Failed to load page, status code: ' + response.statusCode));
                }

                var body = [];

                response.on('data', function (chunk) {
                    body.push(chunk);
                } );

                response.on('end', function () {
                    resolve(body.join(''));
                });
            });

            request.on('error', function(err) {
                reject(err);
            });
        })
    };


    function _getTemperature(callback){
            logger.debug('[DHT11Controller] _getTemperature()');

            var options = {
                hostname: DHT11ControllerConfig.ip,
                port: DHT11ControllerConfig.port,
                path: '/?getdht11readings',
                agent: false
            };

            _getReadingsFromSensor(options)
                .then(function(body) {
                    logger.debug(body);
                    var data = body.split('|');
                    callback(data[0]);
                })
                .catch(function(err) {
                    logger.error(err);
                    callback(false);
                    return false;
                });
    }

    function _getHumidity(callback){
        logger.debug('[DHT11Controller] _getHumidity()');

        var options = {
            hostname: DHT11ControllerConfig.ip,
            port: DHT11ControllerConfig.port,
            path: '/?getdht11readings',
            agent: false
        };

        _getReadingsFromSensor(options)
            .then(function(body) {
                logger.debug(body);
                var data = body.split('|');
                callback(data[1]);
            })
            .catch(function(err) {
                logger.error(err);
                callback(false);
                return false;
            });
    }
    
    function _getTemperatureAndHumidity(callback){
        logger.debug('[DHT11Controller] _getTemperatureAndHumidity()');

        var options = {
            hostname: DHT11ControllerConfig.ip,
            port: DHT11ControllerConfig.port,
            path: '/?getdht11readings',
            agent: false
        };

        _getReadingsFromSensor(options)
            .then(function(body) {
                logger.debug(body);
                var data = body.split('|');
                callback(data);
            })
            .catch(function(err) {
                logger.error(err);
                callback(false);
                return false;
            });
    }

    // Expose API:

    return {
        getTemperature: _getTemperature,
        getHumidity: _getHumidity,
        getTemperatureAndHumidity: _getTemperatureAndHumidity
    };
}) ();

module.exports = DHT11Controller;