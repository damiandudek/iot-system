/*
 * DHT11Controller.ino
 * 
 * Program for Arudino microcontroller.
 * The program read results from DHT11 sensor 
 * and send them to the LCD display
 * or as a response on HTTP request from external parties.
 *
 * Author: Damian Dudek
 * 
*/

/*
 * OPTIONS
 */
// uncomment to enable debug mode
//#define DEBUG

// uncomment to enable DHCP
//#define ENABLE_DHCP

// uncomment to enable ethernet initialization 
// only with MAC and IP address
#define ETHERNET_INIT_ONLY_WITH_MAC_IP

//-------------------------------

#ifdef DEBUG
 #define DEBUG_PRINTLN(x)  Serial.println (x)
 #define DEBUG_PRINT(x)  Serial.print (x)
#else
 #define DEBUG_PRINTLN(x)
 #define DEBUG_PRINT(x)
#endif

#ifdef ETHERNET_INIT_ONLY_WITH_MAC_IP
  #define ETHERNET_BEGIN() Ethernet.begin(mac, ip);
#else
  #define ETHERNET_BEGIN() Ethernet.begin(mac, ip, dnsAddress, gateway, subnet);
#endif

//-------------------------------

/*
 * Ethernet configruation
 */
 
 // INFO: please check settings of system firewall
 // it can block connection

#include <SPI.h>
#include <Ethernet.h>

// MAC address
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// static IP address of the  microcontroller
// (used if the DHCP fails)
IPAddress ip(169, 254, 14, 2);

// DNS services address
// router IP address 
// (or computer if device is connected directly to the computer)
IPAddress dnsAddress(169, 254, 14, 1);

// gateway - ip address of router 
// (or computer if device is connected directly to the computer)
IPAddress gateway(169, 254, 14, 1);

// subnet
IPAddress subnet(255, 255, 0, 0);

// initialize the Ethernet server library
// (port 80 is default for HTTP):
#define SERVER_PORT 80
EthernetServer server(SERVER_PORT);

// used for storing HTTP request content
String httpRequest = String(160);

#ifdef ENABLE_DHCP
  // DHCP connection flag
  bool isDHCP = false;
#endif

//-------------------------------

/*
 * DHT11 Humidity & Temperature Sensor configuration
 */

#include <dht11.h>  

// define data pin for the DHT11 sensor
#define DHT_PIN 7  

DHT11 dht11(DHT_PIN);  

// define min time between readings in ms;  (120000 = 2min)
#define DHT11_TIME_BETWEEN_READINGS 10000  

// timer for dht11
unsigned long dhtTimer;  

//-------------------------------

/*
 * LCD i2c converter configuration
 */

// include libraries for LCD
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE); 

//-------------------------------

/*
 * setup
 */

void setup() {
  
  #ifdef DEBUG
    // open serial communication at 9600 bits per second
    Serial.begin(9600);
    while (!Serial) {
      ; // wait for serial port connection 
    }
  #endif

  /*
   * LCD configuration
   */

  // configure LCD i2C
  lcd.begin(16, 2);
  // enable backlight
  lcd.backlight();
  lcd.clear();
  lcd.home();
  lcd.print("Initialization..");  
  lcd.blink();

  /*
   * DHT11 configuration
   */

  // configure dhtTimer
  dhtTimer = -DHT11_TIME_BETWEEN_READINGS;  // assign value 
  
/*
   * Ethernet 
   */

  #ifdef ENABLE_DHCP
    isDHCP = true;
    // try to get an IP address using DHCP
    DEBUG_PRINTLN("Waiting for an IP address using DHCP...");
    if (Ethernet.begin(mac) == 0) {
      DEBUG_PRINTLN("Failed to configure Ethernet using DHCP");
      isDHCP = false;
      
      // initialize the Ethernet with static IP (not DHCP)
      ETHERNET_BEGIN()
    }
  #else
    // start the Ethernet connection with static IP
    ETHERNET_BEGIN()
  #endif

  // start the server
  server.begin();
  
  // print the Ethernet board/shield's IP address:
  DEBUG_PRINT("My IP address: ");
  DEBUG_PRINTLN(Ethernet.localIP());

  // get first sensor data from dht11 sensor
  if(dht11read())
    sendToLCD();
}

/*
 * loop
 */
 
void loop() {

  #ifdef ENABLE_DHCP
     if(isDHCP)
       // used for refresh IP address using DHCP
       Ethernet.maintain();
   #endif

  // if got sensor data from dht11 then print values on LCD
  if(dht11read())
    sendToLCD();
  
  // listen for new incoming client connects
  EthernetClient client = server.available();
  if (client) {
    DEBUG_PRINTLN("New client is connected.");
    
    // a HTTP request ends with a blank line
    boolean currentLineIsBlank = true;
    
    while (client.connected()) {
      if (client.available()) {
        
        // read HTTP request char by char
        char c = client.read();
        // and save into httpRequest variable
        httpRequest += c;

        // check if HTTP request ends
        // if the line ends (a newline character received)
        // and the line is blank, it means that the HTTP request is completed
        if (c == '\n' && currentLineIsBlank) {
          // send a HTTP 200 response header
          httpResponse200(client);
          
          client.println();
          
          break;
        }
        if (c == '\n') {
          // a new line is starting
          currentLineIsBlank = true;

          // parse HTTP request
          httpRequest.toLowerCase();
          
          if (httpRequest.indexOf("?") < 0) {
            // skip everything
            // looking for parameters in GET query string
            // everything after the question mark (?) in the URL
          } else {
            //print to serial monitor for debugging
            DEBUG_PRINTLN("------------");
            DEBUG_PRINTLN(httpRequest); 
      
            if(httpRequest.indexOf("?getdht11readings") > 0){
              httpResponse200(client);
                String resultToSend = String(dht11.temperature) + "|" + String(dht11.humidity);
                client.println(resultToSend);
            }
          }
          break;
          
        // end PARSE request
        //---------------------------------       
        } else if (c != '\r') {
          // if a character on the current line then set:
          currentLineIsBlank = false;
        }
      }
    }
    
    // give the browser time to receive the data
    delay(1);
    
    // close the open connection
    client.stop();
    DEBUG_PRINTLN("Client disconnected.");
    httpRequest = "";
  }
}

/*
 * read data sensor from DHT11
 */
int8_t errorCode;
int8_t dht11read(){    
  if (millis() > dhtTimer + DHT11_TIME_BETWEEN_READINGS){
    dhtTimer = millis();
    if((errorCode = dht11.measure()) == 0){
      // print for debuging
      DEBUG_PRINT("temperature:");
      DEBUG_PRINT(dht11.temperature);
      DEBUG_PRINT("C");        
      DEBUG_PRINT(" humidity:");
      DEBUG_PRINT(dht11.humidity);
      DEBUG_PRINTLN("%");  
      return true;  
    }
    else{
      DEBUG_PRINTLN();
      DEBUG_PRINT("Error Code:");
      DEBUG_PRINT(errorCode);
      DEBUG_PRINTLN();    
    }
  } 
    
  return false;
}

/*
 * HTTP Response 200
 */

void httpResponse200(EthernetClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");  // close connection after when the response is completed
  client.println(); // HTTP respond ends with new line '\n'
}

/*
 * send data to LCD
 */
void sendToLCD(){
    // display humidity and temperature on the LCD
    lcd.clear();
    lcd.print("Temp: ");  
    lcd.print(dht11.temperature);
    lcd.print((char)223);
    lcd.print("C");
    lcd.setCursor(0, 1);
    lcd.print ("Humi: ");
    lcd.print (dht11.humidity);
    lcd.print("%");
}
