'use strict';

/*
 * Database connection
 */
var databaseAddress = 'mongodb://localhost:27017/iotdb';
var db = require('mongoose');

// open MongoDB pooling connections
db.connect(databaseAddress);

var Device = require('./models/deviceSchema');
var Rule = require('./models/ruleSchema');

var logger = require('winston');
logger.remove(logger.transports.Console);
logger.add(logger.transports.Console, {
    level: 'info',
    prettyPrint: true,
    colorize: true,
    silent: false,
    timestamp: false
});
logger.cli();

var loader = require('./loader');

var dropDevicesCollection = function () {
    logger.info('[Remove collection] dropDevicesCollection()');

    return new Promise(function(resolve, reject){
        Device.remove({}, function(err) {
            if (err) {
                logger.debug(err);
                reject(err);
            }

            logger.info('[Remove collection] Devices collection removed');
            resolve();
        });
    });

};

var dropRulesCollection = function (){
    logger.info('[Remove collection] dropRulesCollection()');

    return new Promise(function(resolve, reject){
        Rule.remove({}, function(err) {
            if (err) {
                logger.debug(err);
                reject(err);
            }

            logger.info('[Remove collection] Rules collection removed');
            resolve()
        });
    });


};

var saveDevicesConfigurationToDB = function () {
    logger.info('[install] saveDevicesConfigurationToDB()');

    return new Promise(function(resolve, reject) {
        var devicesConfiguration = loader.devicesConfiguration;
        var newDeviceToSave = {};

        for (var i in devicesConfiguration) {
            var newDeviceToSave = new Device(devicesConfiguration[i]);

            newDeviceToSave.save(function (err, data) {
                if (err) {
                    logger.debug(err);
                    reject(err);
                }

                logger.info('[install] New device configuration saved to database.');
                resolve(data);
            });
        }
    });
};

var createSampleRules = function () {
    logger.info('[install] createSampleRules()');

    return new Promise(function(resolve, reject) {
        var rules = [
            {
                "name": "Turn Switch 1 ON",
                "description": "Turn Switch 1 ON when temperature is greater than 20",
                "isEnabled": true,
                "antecedent": {
                    ">": [
                        {
                            "var": "var0"
                        },
                        "20"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getTemperature",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        1,
                        1
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                }
            },
            {
                "name": "Turn Switch 1 OFF",
                "description": "Turn Switch 1 OFF when temperature is lower than 27",
                "isEnabled": true,
                "antecedent": {
                    "<": [
                        {
                            "var": "var0"
                        },
                        "27"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getTemperature",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        1,
                        0
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                }
            },
            {
                "name": "Turn Switch 2 ON",
                "description": "Turn Switch 2 ON when air humidity is greater than 30%",
                "isEnabled": true,
                "antecedent": {
                    ">": [
                        {
                            "var": "var0"
                        },
                        "30"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getHumidity",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        2,
                        1
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                }
            },
            {
                "name": "Turn Switch 2 OFF",
                "description": "Turn Switch 2 OFF when air humidity is lower than 50%",
                "isEnabled": true,
                "antecedent": {
                    "<": [
                        {
                            "var": "var0"
                        },
                        "50"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getHumidity",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        2,
                        0
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                }
            },
            {
                "name": "Turn Switch 3 ON (periodic rule)",
                "description": "Turn Switch 3 ON when temperature is greater than 18",
                "isEnabled": true,
                "antecedent": {
                    ">": [
                        {
                            "var": "var0"
                        },
                        "18"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getTemperature",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        3,
                        1
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                },
                "runEvery": 5000
            },
            {
                "name": "Turn Switch 3 OFF (periodic rule)",
                "description": "Turn Switch 3 OFF when temperature is lower than 30",
                "isEnabled": true,
                "antecedent": {
                    "<": [
                        {
                            "var": "var0"
                        },
                        "30"
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getTemperature",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        3,
                        0
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                },
                "runEvery": 7000
            },
            {
                "name": "Turn Switch 1 ON",
                "description": "Turn Switch 1 ON when temperature is lower than air humidity",
                "isEnabled": true,
                "antecedent": {
                    "<": [
                        {
                            "var": "var0"
                        },
                        {
                            "var": "var1"
                        }
                    ]
                },
                "antecedentData": [
                    {
                        "functionToCall": "getTemperature",
                        "deviceId": "57fba92d600f1338ea8598de"
                    },
                    {
                        "functionToCall": "getHumidity",
                        "deviceId": "57fba92d600f1338ea8598de"
                    }
                ],
                "consequent": {
                    "params": [
                        1,
                        1
                    ],
                    "functionToCall": "sendSignaltoSwitch",
                    "deviceId": "57fba92d600f1338ea8598dd"
                },
                "runEvery": 0
            }
        ];

        Rule.insertMany(rules, function (err, data) {
                if (err) {
                    logger.debug(err);
                    reject(err);
                }

                logger.info('[install] New rules saved to database.');
                resolve(data);
            });
    });
};

dropDevicesCollection()
    .then(function () {
        return dropRulesCollection();
    })
    .then(function () {
        return saveDevicesConfigurationToDB();
    })
    .then(function () {
        if(process.argv[2] === '--createSampleRules'){
            return createSampleRules();
        }
    })
    .then(function () {
        process.exit();
    })
    .catch(function (error) {
        logger.debug(error);
    });
