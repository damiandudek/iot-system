/*
 * remoteControl.ino
 *
 * Program for Arudino microcontroller.
 * 
 * The program allows to control remote devices (switches) 
 * via 433MHz signals sending by connected transmitter.
 * Signal to send depends on recievied HTTP request.
 * 
 * Author: Damian Dudek
 *
*/

/*
 * OPTIONS
 */
// uncomment to enable debug mode
//#define DEBUG

// uncomment to enable DHCP
//#define ENABLE_DHCP

// uncomment to enable ethernet initialization 
// only with MAC and IP address
#define ETHERNET_INIT_ONLY_WITH_MAC_IP

//-------------------------------

#ifdef DEBUG
 #define DEBUG_PRINTLN(x)  Serial.println (x)
 #define DEBUG_PRINT(x)  Serial.print (x)
#else
 #define DEBUG_PRINTLN(x)
 #define DEBUG_PRINT(x)
#endif

#ifdef ETHERNET_INIT_ONLY_WITH_MAC_IP
  #define ETHERNET_BEGIN() Ethernet.begin(mac, ip);
#else
  #define ETHERNET_BEGIN() Ethernet.begin(mac, ip, dnsAddress, gateway, subnet);
#endif

//-------------------------------

/*
 * Ethernet configruation
 */
 
 // INFO: please check settings of system firewall
 // it can block connection
 
#include <SPI.h>
#include <Ethernet.h>

// MAC address
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

// static IP address of the  microcontroller
// (used if the DHCP fails)
IPAddress ip(169, 254, 14, 2);

// DNS services address
// router IP address 
// (or computer if device is connected directly to the computer)
IPAddress dnsAddress(169, 254, 14, 1);

// gateway - ip address of router 
// (or computer if device is connected directly to the computer)
IPAddress gateway(169, 254, 14, 1);

// subnet
IPAddress subnet(255, 255, 0, 0);

// initialize the Ethernet server library
// (port 80 is default for HTTP):
#define SERVER_PORT 80
EthernetServer server(SERVER_PORT);

// used for storing HTTP request content
String httpRequest = String(160);

#ifdef ENABLE_DHCP
  // DHCP connection flag
  bool isDHCP = false;
#endif

//-------------------------------

/*
 * RF 433MHz transmitter and receiver configuration
 */
 
#include <RCSwitch.h>

// define pin numbers for the transmitter and receiver
#define TRANSMITTER_PIN 10
#define RECEIVER_PIN 10

RCSwitch RCSwitchController = RCSwitch();

//-------------------------------

/*
 * setup
 */

void setup() {

  #ifdef DEBUG
    // open serial communication at 9600 bits per second
    Serial.begin(9600);
    while (!Serial) {
      ; // wait for serial port connection 
    }
  #endif

  /*
   * RC Swtich library
   */

  // RC SWITH -RF TRANSMITTER, RECIVER
  RCSwitchController.enableTransmit(TRANSMITTER_PIN);

  // (Optional) pulse length
  RCSwitchController.setPulseLength(200);
  
  // (Optional) protocol - default is 1
  // RCSwitchController.setProtocol(1);
  
  // (Optional) number of transmission repetitions.
  // RCSwitchController.setRepeatTransmit(30);

  /*
   * Ethernet 
   */

  #ifdef ENABLE_DHCP
    isDHCP = true;
    // try to get an IP address using DHCP
    DEBUG_PRINTLN("Waiting for an IP address using DHCP...");
    if (Ethernet.begin(mac) == 0) {
      DEBUG_PRINTLN("Failed to configure Ethernet using DHCP");
      isDHCP = false;
      
      // initialize the Ethernet with static IP (not DHCP)
      ETHERNET_BEGIN()
    }
  #else
    // start the Ethernet connection with static IP
    ETHERNET_BEGIN()
  #endif

  // start the server
  server.begin();
  
  // print the Ethernet board/shield's IP address:
  DEBUG_PRINT("My IP address: ");
  DEBUG_PRINTLN(Ethernet.localIP());
}

/*
 * loop
 */

void loop() {
  
   #ifdef ENABLE_DHCP
     if(isDHCP)
       // used for refresh IP address using DHCP
       Ethernet.maintain();
   #endif
   
  // listen for new incoming client connections
  EthernetClient client = server.available();
  if (client) {
    DEBUG_PRINTLN("New client is connected.");
    
    // a HTTP request ends with a blank line
    boolean currentLineIsBlank = true;
    
    while (client.connected()) {
      if (client.available()) {
        
        // read HTTP request char by char
        char c = client.read();
        // and save into httpRequest variable
        httpRequest += c;

        // check if HTTP request ends
        // if the line ends (a newline character received)
        // and the line is blank, it means that the HTTP request is completed
        if (c == '\n' && currentLineIsBlank) {
          // send a HTTP 200 response header
          httpResponse200(client);
          
          client.println();
          
          break;
        }
        if (c == '\n') {
          // a new line is starting
          currentLineIsBlank = true;

          // parse HTTP request
          httpRequest.toLowerCase();
          
          if (httpRequest.indexOf("?") < 0) {
            // skip everything
            // looking for parameters in GET query string
            // everything after the question mark (?) in the URL
          } else {
            //print to serial monitor for debugging
            DEBUG_PRINTLN("------------");
            DEBUG_PRINTLN(httpRequest); 

            if(httpRequest.indexOf("?switch1on") > 0){
              DEBUG_PRINTLN("Turn switch 1 on");
              RCSwitchController.send(1135923, 24);
              httpResponse200(client);
              client.println("success");
            }
          
            if(httpRequest.indexOf("?switch1off") > 0){
              DEBUG_PRINTLN("Turn switch 1 off");
              RCSwitchController.send(1135932, 24);
              httpResponse200(client);
              client.println("success");
            }
          
            if(httpRequest.indexOf("?switch2on") > 0){
              DEBUG_PRINTLN("Turn switch 2 on");
              RCSwitchController.send(1136067, 24);
              httpResponse200(client);
              client.println("success");
            }
          
            if(httpRequest.indexOf("?switch2off") > 0){
              DEBUG_PRINTLN("Turn switch 2 off");
              RCSwitchController.send(1136076, 24);
              httpResponse200(client);
              client.println("success");
            }
          
            if(httpRequest.indexOf("?switch3on") > 0){
              DEBUG_PRINTLN("Turn switch 3 on");
              RCSwitchController.send(1136387, 24);
              httpResponse200(client);
              client.println("success");
            }
          
            if(httpRequest.indexOf("?switch3off") > 0){
              DEBUG_PRINTLN("Turn switch 3 off");
              RCSwitchController.send(1136396, 24);
              httpResponse200(client);
              client.println("success");
            } 
          }
      
          break;

        // end PARSE request
        //---------------------------------       
        } else if (c != '\r') {
          // if a character on the current line then set:
          currentLineIsBlank = false;
        }
      }
    }
    
    // give the browser time to receive the data
    delay(1);
    
    // close the open connection
    client.stop();
    DEBUG_PRINTLN("Client disconnected.");
    
    httpRequest = "";
  }
}

/*
 * HTTP Response 200
 */

void httpResponse200(EthernetClient client) {
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println("Connection: close");  // close connection after when the response is completed
  client.println(); // HTTP respond ends with new line '\n'
}

